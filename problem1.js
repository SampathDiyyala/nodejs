const pro=require('./inventory')
function findId(inventory,CarId){
    if((typeof(CarId)==='undefined') || (typeof(inventory)==='undefined') || (inventory.length ===0)){
        return []
    }
    if(inventory.length>0){
        for(let i=0;i<inventory.length;i++){
            if(inventory[i].id===CarId){
                return inventory[i]
            }
        }
    }
}
module.exports=findId